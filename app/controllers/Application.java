package controllers;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.main;

public class Application extends Controller {

	public static Result index() {
		return ok(main.render());
	}

	public static Result valid() {
		//公私key 請到 https://www.google.com/recaptcha/intro/index.html 申請
		
		String clientIP = request().remoteAddress();

		ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
		reCaptcha.setPrivateKey("6LdxvvcSAAAAAK5KMC-EwHNMIvLwh824dENLPBf4");

		DynamicForm form = Form.form().bindFromRequest();

		String challenge = form.get("recaptcha_challenge_field");
		String uresponse = form.get("recaptcha_response_field");
		
		System.out.println(clientIP);
		System.out.println(challenge);
		System.out.println(uresponse);

		ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(clientIP,
				challenge, uresponse);
		
        if (reCaptchaResponse.isValid()) {
        	return ok("Right answer");
        } else {
        	return ok("Wrong answer");
        }
		
	}
}
